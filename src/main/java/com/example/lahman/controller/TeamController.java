package com.example.lahman.controller;

import com.example.lahman.exception.ResourceNotFoundException;
import com.example.lahman.model.Team;
import com.example.lahman.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/teams")
public class TeamController {
  @Autowired
  TeamRepository teamRepository;

  @GetMapping("/")
  public Iterable<Team> getAllTeams() {
    return teamRepository.findAll();
  }

  @GetMapping("/{teamID}")
  public Team getTeamById(@PathVariable(value = "teamID") String teamID) {
    return teamRepository.findById(teamID).orElseThrow(()->new ResourceNotFoundException("Team", "teamID", teamID));
  }

  /*@GetMapping("/{teamID}/players/{yearID}")
  public Team getTeamById(@PathVariable(value = "teamID") String teamID, @Valid @RequestBody Team teamDetails) {
    return teamRepository.findById(team);
  }*/
}
