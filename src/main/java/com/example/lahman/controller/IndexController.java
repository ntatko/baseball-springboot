package com.example.lahman.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@RequestMapping()
public class IndexController {

  @RequestMapping("/")
  public String sayHello() {
    return "This is the Lahman API. It's here to help you when you're bored.";
  }
}
