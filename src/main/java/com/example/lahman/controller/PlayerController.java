package com.example.lahman.controller;

import com.example.lahman.exception.ResourceNotFoundException;
import com.example.lahman.model.Player;
import com.example.lahman.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/players/")
public class PlayerController {
  @Autowired
  PlayerRepository playerRepository;
  @Autowired
  StatsRepository statsRepository;

  @GetMapping("/")
  public Iterable<Player> getAllPlayers() {
    return playerRepository.findAll();
  }

  @GetMapping("/{playerID}")
  public Player getPlayerById(@PathVariable(value = "playerID") String playerID) {
    return playerRepository.findByPlayerID(playerID);
  }

  @GetMapping("/{playerID}/stats/{year}")
  public YearStats getStatsByYear(@PathVariable(value = "playerID") String playerID, @PathVariable(value="yearID") int yearID, @Valid @RequestBody Player playerDetails) {
    return statsRepository.findStatsById(playerID, yearID);
  }
}
