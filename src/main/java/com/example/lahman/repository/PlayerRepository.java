package com.example.lahman.repository;

import com.example.lahman.model.Player;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends JpaRepository<Player, String> {

  @Query("SELECT p FROM Player AS p WHERE p.playerID = :playerID")
  public Player findByPlayerID(@Param("playerID") String playerID);
}

@Repository
public interface StatsRepository extends JpaRepository<YearStats, String> {

  @Query("SELECT a FROM YearStats AS a WHERE a.playerID = :playerID")
  public YearStats findStatsById(@Param("playerID") String playerID, @Param("yearID") int yearID) 
}
