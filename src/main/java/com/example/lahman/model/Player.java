package com.example.lahman.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name="master")
//@SecondaryTable(name="appearance")
@EntityListeners(AuditingEntityListener.class)
public class Player {

  @Id
  @Column(name = "playerID")
  private String playerID;
  @Column(name = "managerID")
  private String managerID;
  @Column(name = "hofID")
  private String hofID;
  @Column(name = "birthYear")
  private int birthYear;
  @Column(name = "birthMonth")
  private int birthMonth;
  @Column(name = "birthDay")
  private int birthDay;
  @Column(name = "birthCountry;")
  private String birthCountry;
  @Column(name = "birthState")
  private String birthState;
  @Column(name = "birthCity")
  private String birthCity;
  @Column(name = "deathYear")
  private int deathYear;
  @Column(name = "deathMonth")
  private int deathMonth;
  @Column(name = "deathDay")
  private int deathDay;
  @Column(name = "deathCountry")
  private String deathCountry;
  @Column(name = "deathState")
  private String deathState;
  @Column(name = "deathCity")
  private String deathCity;
  @Column(name = "nameFirst")
  private String nameFirst;
  @Column(name = "nameLast")
  private String nameLast;
  @Column(name = "nameNote")
  private String nameNote;
  @Column(name = "nameGiven")
  private String nameGiven;
  @Column(name = "nameNick")
  private String nameNick;
  @Column(name = "weight")
  private int weight;
  @Column(name = "height")
  private double height;
  @Column(name = "bats")
  private char bats;
  @Column(name = "throws")
  private char throwS;
  @Column(name = "debut")
  private String debut;
  @Column(name = "finalGame")
  private String finalGame;
  @Column(name = "college")
  private String college;
  @Column(name = "lahman40ID")
  private String lahman40ID;
  @Column(name = "lahman45ID")
  private String lahman45ID;
  @Column(name = "retroID")
  private String retroID;
  @Column(name = "holtzID")
  private String holtzID;
  @Column(name = "bbrefID")
  private String bbrefID;
  @Column(name = "lahmanID")
  private int lahmanID;

  public String getPlayerID() {
    return this.playerID;
  }

  public void setPlayerID(String playerID) {
    this.playerID = playerID;
  }

  public String getManagerID() {
    return this.managerID;
  }

  public void setManagerID(String managerID) {
    this.managerID = managerID;
  }

  public String getHofID() {
    return this.hofID;
  }

  public void setHofID(String hofID) {
    this.hofID = hofID;
  }

  public int getBirthYear() {
    return this.birthYear;
  }

  public void setBirthYear(int birthYear) {
    this.birthYear = birthYear;
  }

  public int getBirthMonth() {
    return this.birthMonth;
  }

  public void setBirthMonth(int birthMonth) {
    this.birthMonth = birthMonth;
  }

  public int getBirthDay() {
    return this.birthDay;
  }

  public void setBirthDay(int birthDay) {
    this.birthDay = birthDay;
  }

  public String getBirthCountry() {
    return this.birthCountry;
  }

  public void setBirthCountry(String birthCountry) {
    this.birthCountry = birthCountry;
  }

  public String getBirthState() {
    return this.birthState;
  }

  public void setBirthState(String birthState) {
    this.birthState = birthState;
  }

  public String getBirthCity() {
    return this.birthCity;
  }

  public void setBirthCity(String birthCity) {
    this.birthCity = birthCity;
  }

  public int getDeathYear() {
    return this.deathYear;
  }

  public void setDeathYear(int deathYear) {
    this.deathYear = deathYear;
  }

  public int getDeathMonth() {
    return this.deathMonth;
  }

  public void setDeathMonth(int deathMonth) {
    this.deathMonth = deathMonth;
  }

  public int getDeathDay() {
    return this.deathDay;
  }

  public void setDeathDay(int deathDay) {
    this.deathDay = deathDay;
  }

  public String getDeathCountry() {
    return this.deathCountry;
  }

  public void setDeathCountry(String deathCountry) {
    this.deathCountry = deathCountry;
  }

  public String getDeathState() {
    return this.deathState;
  }

  public void setDeathState(String deathState) {
    this.deathState = deathState;
  }

  public String getDeathCity() {
    return this.deathCity;
  }

  public void setDeathCity(String deathCity) {
    this.deathCity = deathCity;
  }

  public String getNameFirst() {
    return this.nameFirst;
  }

  public void setNameFirst(String nameFirst) {
    this.nameFirst = nameFirst;
  }

  public String getNameLast() {
    return this.nameLast;
  }

  public void setNameLast(String nameLast) {
    this.nameLast = nameLast;
  }

  public String getNameNote() {
    return this.nameNote;
  }

  public void setNameNote(String nameNote) {
    this.nameNote = nameNote;
  }

  public String getNameGiven() {
    return this.nameGiven;
  }

  public void setNameGiven(String nameGiven) {
    this.nameGiven = nameGiven;
  }

  public String getNameNick() {
    return this.nameNick;
  }

  public void setNameNick(String nameNick) {
    this.nameNick = nameNick;
  }

  public int getWeight() {
    return this.weight;
  }

  public void setWeight(int weight) {
    this.weight = weight;
  }

  public double getHeight() {
    return this.height;
  }

  public void setHeight(double height) {
    this.height = height;
  }

  public char getBats() {
    return this.bats;
  }

  public void setBats(char bats) {
    this.bats = bats;
  }

  public char getThrowS() {
    return this.throwS;
  }

  public void setThrowS(char throwS) {
    this.throwS = throwS;
  }

  public String getDebut() {
    return this.debut;
  }

  public void setDebut(String debut) {
    this.debut = debut;
  }

  public String getFinalGame() {
    return this.finalGame;
  }

  public void setFinalGame(String finalGame) {
    this.finalGame = finalGame;
  }

  public String getCollege() {
    return this.college;
  }

  public void setCollege(String college) {
    this.college = college;
  }

  public String getLahman40ID() {
    return this.lahman40ID;
  }

  public void setLahman40ID(String lahman40ID) {
    this.lahman40ID = lahman40ID;
  }

  public String getLahman45ID() {
    return this.lahman45ID;
  }

  public void setLahman45ID(String lahman45ID) {
    this.lahman45ID = lahman45ID;
  }

  public String getRetroID() {
    return this.retroID;
  }

  public void setRetroID(String retroID) {
    this.retroID = retroID;
  }

  public String getHoltzID() {
    return this.holtzID;
  }

  public void setHoltzID(String holtzID) {
    this.holtzID = holtzID;
  }

  public String getBbrefID() {
    return this.bbrefID;
  }

  public void setBbrefID(String bbrefID) {
    this.bbrefID = bbrefID;
  }
}

@Entity
@Table(name="appearances")
public class YearStats {
  @Column(name="yearID")
  private int yearID;
  @Column(name="teamID")
  private String teamID;
  @Column(name="lgID")
  private String lgID;
  @Column(name="playerID")
  private String playerID;
  @Column(name="G_all")
  private int G_all;
  @Column(name="G_batting")
  private int G_batting;
  @Column(name="G_defense")
  private int G_defense;
  @Column(name="G_p")
  private int G_p;
  @Column(name="G_c")
  private int G_c;
  @Column(name="G_1b")
  private int G_1b;
  @Column(name="G_2b")
  private int G_2b;
  @Column(name="G_3b")
  private int G_3b;
  @Column(name="G_ss")
  private int G_ss;
  @Column(name="G_lf")
  private int G_lf;
  @Column(name="G_cf")
  private int G_cf;
  @Column(name="G_rf")
  private int G_rf;
  @Column(name="G_of")
  private int G_of;
  @Column(name="G_dh")
  private int G_dh;
  @Column(name="G_ph")
  private int G_ph;
  @Column(name="G_pr")
  private int G_pr;
  public void setYearID(int yearID) {
    this.yearID = yearID;
  }
  public int getYearID() {
    return this.yearID;
  }
  public void setTeamID(String teamID) {
    this.teamID = teamID;
  }
  public String getTeamID() {
    return this.teamID;
  }
  public void setLgID(String lgID) {
    this.lgID = lgID;
  }
  public String getLgID() {
    return this.lgID;
  }
  public void setPlayerID(String playerID) {
    this.playerID = playerID;
  }
  public String getPlayerID() {
    return this.playerID;
  }
  public void setG_all(int G_all) {
    this.G_all = G_all;
  }
  public int getG_all() {
    return this.G_all;
  }
  public void setG_batting(int G_batting) {
    this.G_batting = G_batting;
  }
  public int getG_batting() {
    return this.G_batting;
  }
  public void setG_defense(int G_defense) {
    this.G_defense = G_defense;
  }
  public int getG_defense() {
    return this.G_defense;
  }
  public void setG_p(int G_p) {
    this.G_p = G_p;
  }
  public int getG_p() {
    return this.G_p;
  }
  public void setG_c(int G_c) {
    this.G_c = G_c;
  }
  public int getG_c() {
    return this.G_c;
  }
  public void setG_1b(int G_1b) {
    this.G_1b = G_1b;
  }
  public int getG_1b() {
    return this.G_1b;
  }
  public void setG_2b(int G_2b) {
    this.G_2b = G_2b;
  }
  public int getG_2b() {
    return this.G_2b;
  }
  public void setG_3b(int G_3b) {
    this.G_3b = G_3b;
  }
  public int getG_3b() {
    return this.G_3b;
  }
  public void setG_ss(int G_ss) {
    this.G_ss = G_ss;
  }
  public int getG_ss() {
    return this.G_ss;
  }
  public void setG_lf(int G_lf) {
    this.G_lf = G_lf;
  }
  public int getG_lf() {
    return this.G_lf;
  }
  public void setG_cf(int G_cf) {
    this.G_cf = G_cf;
  }
  public int getG_cf() {
    return this.G_cf;
  }
  public void setG_rf(int G_rf) {
    this.G_rf = G_rf;
  }
  public int getG_rf() {
    return this.G_rf;
  }
  public void setG_of(int G_of) {
    this.G_of = G_of;
  }
  public int getG_of() {
    return this.G_of;
  }
  public void setG_dh(int G_dh) {
    this.G_dh = G_dh;
  }
  public int getG_dh() {
    return this.G_dh;
  }
  public void setG_ph(int G_ph) {
    this.G_ph = G_ph;
  }
  public int getG_ph() {
    return this.G_ph;
  }
  public void setG_pr(int G_pr) {
    this.G_pr = G_pr;
  }
  public int getG_pr() {
    return this.G_pr;
  }
}
