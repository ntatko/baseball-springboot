package com.example.lahman.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "teams")
@EntityListeners(AuditingEntityListener.class)
public class Team {

  @Column(name = "lgID")
  private String lgID;
  @Id
  @Column(name = "teamID")
  private String teamID;
  @Column(name = "franchID")
  private String franchID;
  @Column(name = "divID")
  private char divID;
  @Column(name = "Rank")
  private int Rank;
  @Column(name = "g")
  private int g;
  @Column(name = "GHome")
  private int GHome;
  @Column(name = "W")
  private int W;
  @Column(name = "L")
  private int L;
  @Column(name = "DivWin")
  private char DivWin;
  @Column(name = "WCWin")
  private char WCWin;
  @Column(name = "LgWin")
  private char LgWin;
  @Column(name = "WSWin")
  private char WSWin;
  @Column(name = "R")
  private int R;
  @Column(name = "AB")
  private int AB;
  @Column(name = "H")
  private int H;
  @Column(name = "2B")
  private int secondBase;
  @Column(name = "3B")
  private int threeBase;
  @Column(name = "HR")
  private int HR;
  @Column(name = "BB")
  private int BB;
  @Column(name = "SO")
  private int SO;
  @Column(name = "SB")
  private int SB;
  @Column(name = "CS")
  private int CS;
  @Column(name = "HBP")
  private int HBP;
  @Column(name = "SF")
  private int SF;
  @Column(name = "RA")
  private int RA;
  @Column(name = "ER")
  private int ER;
  @Column(name = "ERA")
  private double ERA;
  @Column(name = "CG")
  private int CG;
  @Column(name = "SHO")
  private int SHO;
  @Column(name = "SV")
  private int SV;
  @Column(name = "IPouts")
  private int IPouts;
  @Column(name = "HA")
  private int HA;
  @Column(name = "HRA")
  private int HRA;
  @Column(name = "BBA")
  private int BBA;
  @Column(name = "SOA")
  private int SOA;
  @Column(name = "E")
  private int E;
  @Column(name = "DP")
  private int DP;
  @Column(name = "FP")
  private double FP;
  @Column(name = "name")
  private String name;
  @Column(name = "park")
  private String park;
  @Column(name = "attendance")
  private int attendance;
  @Column(name = "BPF")
  private int BPF;
  @Column(name = "PPF")
  private int PPF;
  @Column(name = "teamIDBR")
  private String teamIDBR;
  @Column(name = "teamIDlahman45")
  private String teamIDlahman45;
  @Column(name = "teamIDretro")
  private String teamIDretro;

  private int yearID;

  public String getLgID() {
    return this.lgID;
  }

  public void setLgID(String lgID) {
    this.lgID = lgID;
  }

  public String getTeamID() {
    return this.teamID;
  }

  public void setTeamID(String teamID) {
    this.teamID = teamID;
  }

  public String getFranchID() {
    return this.franchID;
  }

  public void setFranchID(String franchID) {
    this.franchID = franchID;
  }

  public char getDivID() {
    return this.divID;
  }

  public void setDivID(char divID) {
    this.divID = divID;
  }

  public int getRank() {
    return this.Rank;
  }

  public void setRank(int rank) {
    this.Rank = rank;
  }

  public int getG() {
    return this.g;
  }

  public void setG(int g) {
    this.g = g;
  }

  public int getGHome() {
    return this.GHome;
  }

  public void setGHome(int GHome) {
    this.GHome = GHome;
  }

  public int getW() {
    return this.W;
  }

  public void setW(int w) {
    this.W = w;
  }

  public int getL() {
    return this.L;
  }

  public void setL(int l) {
    this.L = l;
  }

  public char getDivWin() {
    return this.DivWin;
  }

  public void setDivWin(char divWin) {
    this.DivWin = divWin;
  }

  public char getWCWin() {
    return this.WCWin;
  }

  public void setWCWin(char WCWin) {
    this.WCWin = WCWin;
  }

  public char getLgWin() {
    return this.LgWin;
  }

  public void setLgWin(char lgWin) {
    this.LgWin = lgWin;
  }

  public char getWSWin() {
    return this.WSWin;
  }

  public void setWSWin(char WSWin) {
    this.WSWin = WSWin;
  }

  public int getR() {
    return this.R;
  }

  public void setR(int r) {
    this.R = r;
  }

  public int getAB() {
    return this.AB;
  }

  public void setAB(int AB) {
    this.AB = AB;
  }

  public int getH() {
    return this.H;
  }

  public void setH(int h) {
    this.H = h;
  }

  public int getSecondBase() {
    return this.secondBase;
  }

  public void setSecondBase(int secondBase) {
    this.secondBase = secondBase;
  }

  public int getThreeBase() {
    return this.threeBase;
  }

  public void setThreeBase(int threeBase) {
    this.threeBase = threeBase;
  }

  public int getHR() {
    return this.HR;
  }

  public void setHR(int HR) {
    this.HR = HR;
  }

  public int getBB() {
    return this.BB;
  }

  public void setBB(int BB) {
    this.BB = BB;
  }

  public int getSO() {
    return this.SO;
  }

  public void setSO(int SO) {
    this.SO = SO;
  }

  public int getSB() {
    return this.SB;
  }

  public void setSB(int SB) {
    this.SB = SB;
  }

  public int getCS() {
    return this.CS;
  }

  public void setCS(int CS) {
    this.CS = CS;
  }

  public int getHBP() {
    return this.HBP;
  }

  public void setHBP(int HBP) {
    this.HBP = HBP;
  }

  public int getSF() {
    return this.SF;
  }

  public void setSF(int SF) {
    this.SF = SF;
  }

  public int getRA() {
    return this.RA;
  }

  public void setRA(int RA) {
    this.RA = RA;
  }

  public int getER() {
    return this.ER;
  }

  public void setER(int ER) {
    this.ER = ER;
  }

  public double getERA() {
    return this.ERA;
  }

  public void setERA(double ERA) {
    this.ERA = ERA;
  }

  public int getCG() {
    return this.CG;
  }

  public void setCG(int CG) {
    this.CG = CG;
  }

  public int getSHO() {
    return this.SHO;
  }

  public void setSHO(int SHO) {
    this.SHO = SHO;
  }

  public int getSV() {
    return this.SV;
  }

  public void setSV(int SV) {
    this.SV = SV;
  }

  public int getIPouts() {
    return this.IPouts;
  }

  public void setIPouts(int IPouts) {
    this.IPouts = IPouts;
  }

  public int getHA() {
    return this.HA;
  }

  public void setHA(int HA) {
    this.HA = HA;
  }

  public int getHRA() {
    return this.HRA;
  }

  public void setHRA(int HRA) {
    this.HRA = HRA;
  }

  public int getBBA() {
    return this.BBA;
  }

  public void setBBA(int BBA) {
    this.BBA = BBA;
  }

  public int getSOA() {
    return this.SOA;
  }

  public void setSOA(int SOA) {
    this.SOA = SOA;
  }

  public int getE() {
    return this.E;
  }

  public void setE(int e) {
    this.E = e;
  }

  public int getDP() {
    return this.DP;
  }

  public void setDP(int DP) {
    this.DP = DP;
  }

  public double getFP() {
    return this.FP;
  }

  public void setFP(double FP) {
    this.FP = FP;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPark() {
    return this.park;
  }

  public void setPark(String park) {
    this.park = park;
  }

  public int getAttendance() {
    return this.attendance;
  }

  public void setAttendance(int attendance) {
    this.attendance = attendance;
  }

  public int getBPF() {
    return this.BPF;
  }

  public void setBPF(int BPF) {
    this.BPF = BPF;
  }

  public int getPPF() {
    return this.PPF;
  }

  public void setPPF(int PPF) {
    this.PPF = PPF;
  }

  public String getTeamIDBR() {
    return this.teamIDBR;
  }

  public void setTeamIDBR(String teamIDBR) {
    this.teamIDBR = teamIDBR;
  }

  public String getTeamIDlahman45() {
    return this.teamIDlahman45;
  }

  public void setTeamIDlahman45(String teamIDlahman45) {
    this.teamIDlahman45 = teamIDlahman45;
  }

  public String getTeamIDretro() {
    return this.teamIDretro;
  }

  public void setTeamIDretro(String teamIDretro) {
    this.teamIDretro = teamIDretro;
  }
}
