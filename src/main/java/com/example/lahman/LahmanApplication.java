package com.example.lahman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class LahmanApplication {
	public static void main(String[] args) {
		SpringApplication.run(LahmanApplication.class, args);
	}
}
